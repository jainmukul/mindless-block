/**
 * Created by Owner on 6/16/2017.
 */

console.log("note js connected")

///// select the text area

// var note_area = document.getElementById("textarea")
var note_area = document.getElementById('notetext')

//// create a function to save the textarea to local storage
// I'll also want to check & load previously saved notes

// ;(function load_notes () {
//   if (note_area !== null) {
//     note_area.value = chrome.storage.sync.get("stored_obj", function (resultObject) {
//       note_area.value = resultObject
//     })
//   } else {
//     console.log("can not detect notes in the txt area")
//   }
// })()

function save_notes () {
  chrome.storage.sync.set({
    stored_obj: note_area.value
  }, function () {
    console.log("Saved into Chrome Storage")
  })
}

//// setup a blur handler in notes.html to fire function save_notes
// could not be done (inline JS). I need to add a listener and a function in this file

function console_fire() {
  console.log ('fired the console log')
}

note_area.addEventListener('blur', console_fire)



//   })
// } else {
//   console.log ("Either notetext is null or some issue has happened")
// }

// document.querySelector('#notetext').addEventListener('blur', function () {
//   console.log("focus lost")
// })

